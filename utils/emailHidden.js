const hiddingEmail = (email) => {

    let splitter = '@';
    let visible = 3;
    
    let emailSplit = email.split(splitter);
    let firstLetters = emailSplit[0].substring(0, visible);

    let dots = '';

    for (let i = 0; i < emailSplit[0].length-visible; i++) {
        dots += '*';
    }

    return firstLetters + dots + splitter + emailSplit[1];
  }
  
  module.exports = {
      hiddingEmail
  }