const jwt = require('jsonwebtoken');

export const validCheckAnswer = (userProfil, res) => {

    let tokenSecret = process.env.TOKEN_SECRET;

    let expireDateHours = new Date(Date.now() + 8 * 3600000); //expire dans 8 heures
    let expireDateDay = new Date(Date.now() + 24 * 3600000); //expire dans 1 jour
    let expireDateSecondes = new Date(Date.now() + 30000); //expire dans 30 secondes

    let userJWT = jwt.sign({
            userId: userProfil.id,
            userPw: userProfil.password,
            userStatus: userProfil.status
        }, tokenSecret);

    res
        .cookie('id', userJWT, {httpOnly: true, maxAge: expireDateSecondes, signed: true})
        .cookie('frontId', 'isHere', {maxAge: expireDateSecondes, signed: false})
        .status(200)
        .json({
            id: userProfil.id,
            status: userProfil.status
        });
}