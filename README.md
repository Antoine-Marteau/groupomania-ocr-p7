# groupomania-ocr-p7

## Présentation du projet

Groupomania est un réseau social interne de l’entreprise Groupomania, permettant une meilleure communication entre les salariés.

Il permet aux salariés de :
- S'enregistrer avec un compte
- Ajouter des posts avec ou sans image
- Réagir à des posts avec un like ou des messages

## Organisation du dépôt

Ce dépôt est composé de deux branches :

- [**Main**](https://gitlab.com/Antoine-Marteau/groupomania-ocr-p7/-/tree/main), contenant la version compilée la plus avancée du projet.
- [**Development**](https://gitlab.com/Antoine-Marteau/groupomania-ocr-p7/-/tree/development), permettant de mettre en place facilement un environnement de développement pour modifier le projet.

## Utiliser la dernière version du projet

1. Cloner la branche Main

        git clone https://gitlab.com/Antoine-Marteau/groupomania-ocr-p7.git main
1. Installer les dépendances npm

        cd main
        npm i
1. Créer le fichier ``.env`` dans l'arborescence principale du projet

1. Créer un dossier ``uploads``, également dans l'arborescence principale du projet

1. Lancer le projet

        npm run start

Accéder au front du projet en suivant ce lien : [localhost:8081/welcome](http://localhost:8081/welcome)

## Contribuer au projet

Afin de profiter de l'écosystème de développement fourni par React, le projet se divise en deux dossiers, afin de distiguer la partie serveur (p7-back) et la partie client (p7-front). Pour pouvoir travailler sur le projet, ces deux parties doivent êtres installées.

### Installer le projet en local

1. Cloner la branche Development

        git clone -b development https://gitlab.com/Antoine-Marteau/groupomania-ocr-p7.git dev
1. Installer les dépendances pour la partie client (p7-front)

        cd dev/p7-front
        npm i
1. Lancer le service pour la partie client

        npm run start
1. Aller dans la partie serveur afin d'installer également les dépendances
        
        cd ../p7-back
        npm i

1. Créer le fichier ``.env`` dans l'arborescence principale du serveur

1. Créer un dossier ``uploads``, également dans l'arborescence principale du serveur

1. Lancer le service pour la partie serveur

        npm run start

Adresse pour accéder au front : [localhost:3000/welcome](http://localhost:3000/welcome)  
Adresse pour accéder au back : [localhost:8081/welcome](http://localhost:8081)

### Mettre à jour la branche Main

Pour mettre à jour la branche Main, il est conseillé de créer une nouvelle arborescence de travail grâce à la commande git worktree, et d'y remplacer les fichiers modifiés.

    git worktree add ../main main   

Le remplacement peut se faire manuellement, ou bien par le biais de scripts (powershell, bash,..).

Pour mettre à jour la partie client sur la branche Main, il faut intégrer les fichiers du build React dans le dossier front.

## Mettre en place la base de donnée

Le projet fonctionne grâce à une base de donnée MySQL. Afin de mettre en place la base de donnée de l'application, après avoir installé MySQL et s'être connecté :

1. Créer une nouvelle base de donnée Groupomania  
        
        CREATE DATABASE groupomania;
1. Créer l'ensemble des tables constituants la base de donné

        CREATE TABLE user (
            surname varchar(50),
            name varchar(50),
            email_hidden varchar(50),
            password varchar(80),
            status varchar(25),
            email varchar(50) NOT NULL PRIMARY KEY
            id varchar(80) UNIQUE KEY
        );
        
        CREATE TABLE likes (
            like_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
            post_id int NOT NULL,
            user_id varchar(80) NOT NULL
        );
        
        CREATE TABLE message (
            content longtext,
            post_id int,
            user_id varchar(80)
            ID int NOT NULL AUTO_INCREMENT PRIMARY KEY
        );
        
        CREATE TABLE post (
            content longtext,
            cat varchar(50),
            user_id varchar(80)
            ID int NOT NULL AUTO_INCREMENT PRIMARY KEY
        );
1. Renseigner les informations de connexion dans le ``.env``.
