const jwt = require('jsonwebtoken');
const mysql = require('mysql2');
const fs = require('fs');

module.exports = (req, res, next) => {

    try {

        let signedCookieId = req.signedCookies['id'];
        let token = process.env.TOKEN_SECRET;
        let decodedToken = jwt.verify(signedCookieId, token);

        res.locals.userId = decodedToken.userId;
        res.locals.userStatus = decodedToken.userStatus;

        next();
        return

    } catch (error) {

        if(req.body.fileName){
            fs.unlinkSync('./uploads/'+req.body.fileName);
        }

        res.redirect('http://localhost:8081');
    }
}