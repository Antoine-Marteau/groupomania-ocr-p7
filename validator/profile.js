const { body } = require('express-validator')

const profilDeleteFields = () => {
  
  return [
    body('password').isString().notEmpty(),
  ]
}

const profilPutFields = () => {
    
    return [
      body('surname').isString().notEmpty(),
      body('name').isString().notEmpty(),
      body('login').isString(),
      body('actualPassword').isString().notEmpty(),
      body('firstPassword').isString(),
      body('secondPassword').isString(),
      body('actualEmail').isString().notEmpty(),
      body('hiddenEmail').isEmail().notEmpty(),
    ]
}

module.exports = {
  profilDeleteFields,
  profilPutFields
}