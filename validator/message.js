const { body } = require('express-validator')

const messageDeleteFields = () => {
  
  return [
    body('messageId').isInt().notEmpty(),
  ]
}

const messagePostFields = () => {
  
  return [
    body('messageContent').isString().notEmpty(),
    body('postId').isInt().notEmpty(),
  ]
}

module.exports = {
    messageDeleteFields,
    messagePostFields
}