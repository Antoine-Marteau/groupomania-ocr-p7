const { body } = require('express-validator')

const signUpFields = () => {
  
  return [
    body('email').isEmail().notEmpty(),
    body('password').isString().notEmpty(),
    body('name').isString().notEmpty(),
    body('surname').isString().notEmpty()
  ]
}

const signInFields = () => {
  
  return [
    body('email').isEmail().notEmpty(),
    body('password').isString().notEmpty()
  ]
}

module.exports = {
  signInFields,
  signUpFields
}