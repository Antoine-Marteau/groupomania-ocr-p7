const { body } = require('express-validator')

const postPostFields = () => {
  
  return [
    body('postCat').isString().notEmpty(),
    body('postContent').isString().notEmpty(),
  ]
}

const putPostFields = () => {
  
  return [
    body('postId').isInt().notEmpty(),
    body('postContent').isString().notEmpty(),
  ]
}

const deletePostFields = () => {
  
  return [
    body('postId').isInt().notEmpty(),
  ]
}

const putLikeFields = () => {
  
  return [
    body('likeId').notEmpty(),
  ]
}

module.exports = {
  postPostFields,
  putPostFields,
  deletePostFields,
  putLikeFields
}