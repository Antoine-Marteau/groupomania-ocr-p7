import express from "express";

import { home, signin, signup, logout } from "../controllers/controllers.js";
import { messageGet, messagePost, messageDelete } from "../controllers/message.js";
import { postPost, postDelete, postPut, postSingle, postLike } from "../controllers/post.js";
import { profileGet, /*profilePost,*/ profileDelete, profilePut } from "../controllers/profile.js";

const path = require('path');
const app = express();

/* MULTER */
const multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        let fileExtension = file.mimetype.split('/')[1];
        let fileName = file.originalname + '-' + Date.now() + '.' + fileExtension;
        cb(null, fileName.replace(/ /gm, '_'));
        req.body.fileName = fileName;
    }
})

var uploadPost = multer({ storage: storage });

const router = express.Router();

const cookieChecker = require('../middleware/cookieChecker');
const formMiddleware = require('../middleware/formValidator');
const { messageDeleteFields, messagePostFields } = require('../validator/message');
const { signInFields, signUpFields } = require('../validator/page');
const { postPostFields, deletePostFields, putPostFields, putLikeFields } = require('../validator/post');
const { profilDeleteFields, profilPutFields } = require('../validator/profile');

/* PAGE */
router.get('/home', cookieChecker, home);
router.get('/logout', logout);
router.post('/signup', signUpFields(), formMiddleware.formValidator, signup);
router.post('/signin', signInFields(), formMiddleware.formValidator, signin);

/* PROFILE */
router.get('/profile/:id', profileGet);
router.delete('/profile', cookieChecker, profilDeleteFields(), formMiddleware.formValidator, profileDelete);
router.put('/profile', cookieChecker, profilPutFields(), formMiddleware.formValidator, profilePut);

/* POST */
router.post('/post', uploadPost.single('postMedia'), cookieChecker, postPostFields(), formMiddleware.formValidator, postPost);
router.put('/post', uploadPost.single('postMedia'), cookieChecker, putPostFields(), formMiddleware.formValidator, postPut);
router.put('/like/:id', cookieChecker, putLikeFields(), formMiddleware.formValidator, postLike);
router.delete('/post', cookieChecker, deletePostFields(), formMiddleware.formValidator, postDelete);
router.get('/post/:id', cookieChecker, postSingle);

/* MESSAGE */
router.get('/message/:id', messageGet);
router.post('/message', cookieChecker, messagePostFields(), formMiddleware.formValidator, messagePost);
router.delete('/message', cookieChecker, messageDeleteFields(), formMiddleware.formValidator, messageDelete);

router.get('*', (req, res) => {                       
    res.sendFile(path.resolve(__dirname, '../front', 'index.html'));                               
  });

export default router;