const mysql = require('mysql2');

export const messageGet = async (req, res) => {

    let postId = req.params.id;

    try {
        const db = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USERDB,
            password: process.env.PASSWORD,
            database: process.env.DATABASE
        });

        db.connect(function (err) {
            if (err) throw err;
        });

        let query = 'SELECT message.content, message.ID, message.user_id, user.name, user.surname FROM message LEFT JOIN user ON message.user_id = user.id WHERE post_id = ' + postId + ';';

        db.query(query, function (error, results, fields) {
            if (error) throw error;
            
            res.status(200).json(results);
        });

    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const messagePost = async (req, res) => {

    let messageContent = req.body.messageContent.replace(/'/mg, "''");
    let postId = req.body.postId;
    let {userId, userStatus} = res.locals;

    if (userStatus === 'normal') {

        try {
            const db = mysql.createConnection({
                host: process.env.HOST,
                user: process.env.USERDB,
                password: process.env.PASSWORD,
                database: process.env.DATABASE
            });

            db.connect(function (err) {
                if (err) throw err;
            });

            let query = "INSERT INTO message (content, user_id, post_id) VALUES ('" + messageContent + "', '" + userId + "', '" + postId + "')";

            db.query(query, function (error, results, fields) {
                if (error) throw error;
                res.status(200).json(results);
            });

        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(401).json({errorMsg: "Vous n'êtes pas authorisé à effectuer cette action !"});
    }
}

export const messageDelete = async (req, res) => {

    let { userId, userStatus } = res.locals;
    let { messageId } = req.body;

    try {
        const db = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USERDB,
            password: process.env.PASSWORD,
            database: process.env.DATABASE
        });

        db.connect(function (err) {
            if (err) throw err;
        });

        let query = (userStatus === 'admin') ? (
            'DELETE FROM message WHERE ID=' + messageId
        ) : (
            "DELETE FROM message WHERE ID=" + messageId + " AND user_id='" + userId + "'"
        );

        db.query(query, function (error, results, fields) {
            if (error) throw error;
            res.status(200).json(results);
        });

    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}