const mysql = require('mysql2');
const bcrypt = require("bcrypt");
const crypto = require('crypto');
import { nanoid } from 'nanoid';
const { hiddingEmail } = require('../utils/emailHidden');
import { validCheckAnswer } from '../utils/validCheckAnswer';

export const home = async (req, res) => {

    try {
        const db = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USERDB,
            password: process.env.PASSWORD,
            database: process.env.DATABASE
        });

        db.connect(function(err) {
            if (err) throw err;
            
        });

        //Selectionne les posts et les auteurs

        let query = "SELECT post.content, post.ID, post.user_id, user.name, user.surname, likes.like_id FROM post LEFT JOIN user ON user_id = user.id LEFT JOIN likes ON post.ID = likes.post_id AND likes.user_id = '" + res.locals.userId + "'";

        db.query(query, function (error, results, fields) {
            if (error) throw error;
            
            
            let response = {
                posts: results,
                userId: res.locals.userId,
                userStatus: res.locals.userStatus
            };
            res.status(200).json(response);
        });

    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const signup = async (req, res, next) => {

    //S'INSCRIRE

    let inputSurname = req.body.surname;
    let inputName = req.body.name;
    let inputEmailInitial = req.body.email;
    let inputEmail = crypto.createHash('sha256').update( req.body.email ).digest('base64');
    let inputPassword = req.body.password;
    let newId = nanoid();

    let baseHash = inputPassword + inputEmail;
    let tokenSecret = process.env.TOKEN_SECRET;

    if(inputEmailInitial === process.env.ADMIN_MAIL){
        res.status(401).json({errorMsg: 'E-mail déjà utilisé !'});
        return;
    }

    bcrypt.hash(baseHash, 5)
        .then(hash => {
            try {
                const db = mysql.createConnection({
                    host: process.env.HOST,
                    user: process.env.USERDB,
                    password: process.env.PASSWORD,
                    database: process.env.DATABASE
                });

                db.connect(function(err) {
                    if (err) throw err;
                    
                });

                // vérification de la valeur
                let queryValue = "SELECT * FROM user WHERE email = '"+inputEmail+"'";
                
                db.query(queryValue, function (error, resultsSignIn, fields) {
                    if (error) throw error;
                    
                    if(resultsSignIn.length === 0){

                        // insertion de la valeur
                        let insertValue = "INSERT INTO user (surname, name, email, password, status, id, email_hidden) VALUES ('"+inputSurname+"','"+inputName+"','"+inputEmail+"','"+hash+"','normal','"+newId+"','"+hiddingEmail(inputEmailInitial)+"') ON DUPLICATE KEY UPDATE password=password";

                        db.query(insertValue, function (error, resultsSignIn, fields) {
                            if (error) throw error;

                            let user = {
                                id: newId,
                                password: hash,
                                status: 'normal'
                            };

                            validCheckAnswer( user, res );
                        });
                    } else {
                        res.status(401).json({errorMsg: 'E-mail déjà utilisé !'});
                    }
                });

            } catch (error) {
                res.status(500).json({ message: error.message });
            }
        })
};

export const signin = async (req, res, next) => {

    //S'IDENTIFIER

    let inputEmail = crypto.createHash('sha256').update( req.body.email ).digest('base64');
    let inputPassword = req.body.password;

    let baseHash = inputPassword + inputEmail;

    if (inputPassword === process.env.ADMIN_PW && req.body.email === process.env.ADMIN_MAIL) {
        let adminUser = {
            status: process.env.ADMIN_STATUS,
            id: process.env.ADMIN_ID,
            password: process.env.ADMIN_PW
        }
        validCheckAnswer( adminUser, res );
        return
    }

    try {
        const db = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USERDB,
            password: process.env.PASSWORD,
            database: process.env.DATABASE
        });
        
        let queryValue = "SELECT email, password, id, status FROM user WHERE email='"+inputEmail+"'";

        db.connect(function(err) {
            if (err) throw err;
            
        });

        db.query(queryValue, function (error, resultsSignIn, fields) {
            if (error) throw error;

            if (resultsSignIn[0]) {
                    bcrypt.compare(baseHash, resultsSignIn[0].password)
                    .then(valid => {
                        if (!valid) {
                            
                            return res.status(401).json({ errorMsg: 'Login ou mot de passe incorrect !' });
                        }
                        validCheckAnswer( resultsSignIn[0], res );
                    })
            }
        })
    
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

export const logout = async (req, res, next) => {

    res
        .clearCookie('frontId')
        .clearCookie('id')
        .json('Successfully disconnected');
};