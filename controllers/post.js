const mysql = require('mysql2');
const fs = require('fs')

/* CONNECT */

export const postSingle = async (req, res) => {

    let postId = req.params.id;

    /* CONNECT */
    try {
        const db = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USERDB,
            password: process.env.PASSWORD,
            database: process.env.DATABASE
        });

        db.connect(function (err) {
            if (err) throw err;
        });

        let query = "SELECT post.content, post.ID, post.user_id, user.name, user.surname FROM post LEFT JOIN user ON user_id = user.id WHERE post.ID = " + postId;

        db.query(query, function (error, results, fields) {
            if (error) throw error;
            res.status(200).json(results);
        });

    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const postPost = async (req, res) => {

    let postContent = req.body.postContent.replace(/'/mg, "''");
    let newPostText = (req.file) ? req.file.filename + '<!--thumbnail-->' + postContent : postContent;
    let newPostCat = req.body.postCat;
    let { userId, userStatus } = res.locals;

    if(userStatus === 'normal'){

        /* CONNECT */
        try {
            const db = mysql.createConnection({
                host: process.env.HOST,
                user: process.env.USERDB,
                password: process.env.PASSWORD,
                database: process.env.DATABASE
            });

            db.connect(function (err) {
                if (err) throw err;
            });

            let query = "INSERT INTO post (content, cat, user_id) VALUES ('" + newPostText + "', '" + newPostCat + "', '" + userId + "')";

            db.query(query, function (error, results, fields) {
                if (error) throw error;
                res.status(200).json(results);
            });

        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(401).send("Vous n'êtes pas authorisé à effectuer cette action !");
    }
}

export const postPut = async (req, res) => {

    let postContent = req.body.postContent.replace(/'/mg, "''");
    let modifiedPostText;
    let {userId, userStatus} = res.locals;
    let postId = req.body.postId;

    if(userStatus === 'normal'){

        if(req.body.originalPicture){
            modifiedPostText = req.body.originalPicture + '<!--thumbnail-->' + postContent;
        } else if(req.body.fileName){
            modifiedPostText = req.file.filename + '<!--thumbnail-->' + postContent;
        } else {
            modifiedPostText = postContent;
        }

        req.body.oldPicture ? (
            fs.unlinkSync('./uploads/' + req.body.oldPicture)
            ) : '' ;

        /* CONNECT */
        try {
            const db = mysql.createConnection({
                host: process.env.HOST,
                user: process.env.USERDB,
                password: process.env.PASSWORD,
                database: process.env.DATABASE
            });

            db.connect(function (err) {
                if (err) throw err;
            });

            let query = "UPDATE post SET content = '"+modifiedPostText+"' WHERE ID = " + postId + " AND user_id = '" + userId + "'";

            db.query(query, function (error, results, fields) {
                if (error) throw error;
                res.status(200).json(results);
            });

        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(401).send("Vous n'êtes pas authorisé à effectuer cette action !");
    }
}

export const postDelete = async (req, res) => {

    let {userId, userStatus} = res.locals;
    let { postId } = req.body;

    try {
        const db = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USERDB,
            password: process.env.PASSWORD,
            database: process.env.DATABASE
        });

        db.connect(function (err) {
            if (err) throw err;
        });

        let selectPostQuery = 'SELECT content FROM post WHERE post.ID =' + postId;
        let deletePostQuery = (userStatus === 'admin') ? (
            'DELETE post, message, likes FROM post LEFT JOIN message ON post.ID = message.post_id LEFT JOIN likes ON post.ID = likes.post_id WHERE post.ID =' + postId
        ) : (
            "DELETE post, message, likes FROM post LEFT JOIN message ON post.ID = message.post_id LEFT JOIN likes ON post.ID = likes.post_id WHERE post.ID =" + postId + " AND post.user_id ='" + userId + "'"
        );

        db.query(selectPostQuery, function (selectError, selectResults, selectFields) {
            if (selectError) throw selectError;
            if (selectResults[0].content.indexOf('<!--thumbnail-->') !== -1) {
                let fileName = selectResults[0].content.split('<!--thumbnail-->')[0];
                fs.unlinkSync('./uploads/' + fileName);
            }
        });

        db.query(deletePostQuery, function (deleteError, deleteResults, deleteFields) {
            if (deleteError) throw deleteError;
        
            res.status(200).json(deleteResults);
        });

    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const postLike = async (req, res) => {
    
    let postId = req.params.id;
    let {likeId} = req.body;
    let {userId, userStatus} = res.locals;

    if (userStatus === 'normal') {

        try {
            const db = mysql.createConnection({
                host: process.env.HOST,
                user: process.env.USERDB,
                password: process.env.PASSWORD,
                database: process.env.DATABASE
            });

            db.connect(function (err) {
                if (err) throw err;
            });

            let query = likeId ? (
                    "DELETE FROM likes WHERE like_id = " + likeId + " AND user_id = '" + userId + "'"
                ) : (
                    "INSERT INTO likes (post_id, user_id) VALUES ('"+postId+"', '"+userId+"')"
                );

            db.query(query, function (error, results, fields) {
                if (error) throw error;
                res.status(200).json(results);
            });

        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(401).send("Vous n'êtes pas authorisé à effectuer cette action !");
    }
}