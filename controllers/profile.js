const mysql = require('mysql2');

const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const { hiddingEmail } = require('../utils/emailHidden');
import { validCheckAnswer } from '../utils/validCheckAnswer';

export const profileGet = async (req, res) => {

    let userId = req.params.id;

    try {
        const db = mysql.createConnection({
            host: process.env.HOST,
            user: process.env.USERDB,
            password: process.env.PASSWORD,
            database: process.env.DATABASE
        });

        db.connect(function (err) {
            if (err) throw err;
        });

        let query = "SELECT surname, name, id, status, email_hidden, email FROM user WHERE id = '" + userId + "'";

        db.query(query, function (error, results, fields) {
            if (error) throw error;
            res.status(200).json(results);
        });

    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

export const profileDelete = async (req, res) => {

    let {userId, userStatus} = res.locals;
    
    if(userStatus === 'normal'){
        try {
            const db = mysql.createConnection({
                host: process.env.HOST,
                user: process.env.USERDB,
                password: process.env.PASSWORD,
                database: process.env.DATABASE
            });
            db.connect(function (err) {
                if (err) throw err;
            });

            let userQuery = "SELECT email, password FROM user WHERE id = '" + userId + "'";
            let deleteUserQuery = "DELETE user, likes FROM user LEFT JOIN likes ON user.id = likes.user_id WHERE id='" + userId + "'";
            let updatePostQuery = "UPDATE post SET user_id='--IAmTheDeletedUser--' WHERE user_id='" + userId + "'";
            let updateMessageQuery = "UPDATE message SET user_id='--IAmTheDeletedUser--' WHERE user_id='" + userId + "'";

            db.query(userQuery, function (userError, userResults, userFields) {
                if (userError) throw userError;

                if (userResults[0]){


                    let baseHash = req.body.password + userResults[0].email;

                    bcrypt.compare(baseHash, userResults[0].password)
                        .then(valid => {
                            if (!valid) {
                                return;
                            }

                            db.query(deleteUserQuery, function (deleteError, deleteResults, deleteFields) {
                                if (deleteError) throw deleteError;
                            
                                db.query(updatePostQuery, function (postError, postResults, postFields) {
                                    if (postError) throw postError;
                                })
                            
                                db.query(updateMessageQuery, function (messageError, messageResults, messageFields) {
                                    if (messageError) throw messageError;
                                })
                            
                                res
                                    .clearCookie("frontId")
                                    .clearCookie("id")
                                    .json(userResults);
                            });
                        })
                } else {

                    return;
                }
            });

        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(401).send("Vous n'êtes pas authorisé à effectuer cette action !");
    }
}

export const profilePut = async (req, res) => {

    let {
        surname,
        name,
        login,
        actualPassword,
        firstPassword,
        secondPassword,
        actualEmail,
        hiddenEmail
    } = req.body;
    let {userId, userStatus} = res.locals;
    
    let baseHash = actualPassword + actualEmail;

    //Récupération du mot de passe et de l'email depuis la BDD
    if(userStatus === 'normal'){
    
        try {
            const db = mysql.createConnection({
                host: process.env.HOST,
                user: process.env.USERDB,
                password: process.env.PASSWORD,
                database: process.env.DATABASE
            });

            db.connect(function (err) {
                if (err) throw err;
            });

            let query = "SELECT password FROM user WHERE id='" + userId + "'";

            db.query(query, function (error, results, fields) {
                if (error) throw error;

                let userPassword = results[0].password;

                //Vérification du mot de passe entré par l'utilisateur
                bcrypt.compare(baseHash, userPassword)
                    .then(valid => {
                        if (!valid) {

                            return res.status(401).json({ error: 'Mot de passe incorrect !' });
                        }

                        let newBaseHash;

                        login ? (
                            hiddenEmail = hiddingEmail(login),
                            login = crypto.createHash('sha256').update( login ).digest('base64')
                        ): login = actualEmail;
                        
                        //Mot de passe à hasher selon les champs remplis
                        if ((firstPassword.length > 0) || (secondPassword.length > 0)) {

                            if(firstPassword !== secondPassword){
                                return
                            }
                            newBaseHash = firstPassword + login;
                        
                        } else {
                            newBaseHash = actualPassword + login;
                        }

                        bcrypt.hash(newBaseHash, 5)
                            .then(hash => {

                                //Mise à jour des infos
                                let updateProfilQuery = "UPDATE user SET surname='"+surname+"', name='"+name+"', email='"+login+"', email_hidden='"+hiddenEmail+"', password='"+hash+"' WHERE id='" + userId + "'";
                                db.query(updateProfilQuery, function (error, results, fields) {
                                    if (error) throw error;

                                    let user = {
                                        id: userId,
                                        password: hash,
                                        status: userStatus
                                    };
                                
                                    validCheckAnswer( user, res );
                                })
                            })
                    })
            });

        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(401).send("Vous n'êtes pas authorisé à effectuer cette action !");
    }
}