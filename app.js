const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');

import productRoutes from "./routes/routes.js";

const app = express();

require('dotenv').config();

let tokenCookie = process.env.TOKEN_COOKIE;

app.use(cookieParser(tokenCookie));
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.use((req, res, next) => {
  
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  res.header('Access-Control-Allow-Credentials', 'true');

  next();
});

app.use('/front', express.static(path.join(__dirname, 'front')));
app.use(express.static('front'));

app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use(express.static('uploads'));

app.use('/', productRoutes);

module.exports = app;